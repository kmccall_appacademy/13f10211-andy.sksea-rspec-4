class MyHashSet
  attr_reader :store
  
  def initialize(h=nil)
    @store = h.nil? ? {} : h
  end
  
  def insert(el)
    @store[el] = true
  end
  
  def include?(el)
    @store.keys.include?(el)
  end
  
  def delete(el)
    @store.delete(el)
  end
  
  def to_a
    @store.keys
  end
  
  def union(set)
    store2 = set.store
    res = Marshal.load(Marshal.dump(@store))
    store2.each { |k, v| res[k] = v if !res.include?(k) }
    MyHashSet.new(res)
  end
  
  def intersect(set)
    store2 = set.store
    res = @store.select { |k, _| store2.include?(k) }
    MyHashSet.new(res)  
  end
  
  def minus(set)
    store2 = set.store
    res = @store.select { |k, _| !store2.include?(k) }
    MyHashSet.new(res)
  end
  
  def symmetric_difference(set)
    self.minus(set).union(set.minus(self))
  end
  
  def ==(set)
    store2 = set.store
    return false if @store.length != store2.length
    @store.each { |k, v| return false if v != store2[k] }
    true
  end
  
end

# Bonus
#
# - Write a `set1#symmetric_difference(set2)` method; it should return the
#   elements contained in either `set1` or `set2`, but not both!
# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
