class Timer
  attr_reader :seconds
  
  def initialize
    @seconds = 0
  end
  
  def seconds=(s)
    raise ArgumentError.new("Assign an integer only") if s.class != Fixnum
    @seconds = s
  end
  
  def time_string
    i = 0
    res = []
    while 60**i <= @seconds
      res << @seconds / 60**i % 60
      i += 1
    end
    res << 0 until res.length == 3
    res.map! { |t| t < 10 ? "0#{t}" : t.to_s }
    res.reverse.join(":")
  end
  
end