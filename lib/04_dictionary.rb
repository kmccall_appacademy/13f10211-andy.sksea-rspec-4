class Dictionary
  attr_reader :entries
  
  def initialize
    @entries = {}
  end
  
  def add(entry)
    if entry.class == Hash
      @entries[entry.keys[0]] = entry.values[0]
    elsif entry.class == String
      @entries[entry] = nil
    end
  end
  
  def keywords
    @entries.keys.sort
  end
  
  def include?(k)
    self.keywords.include?(k)
  end
  
  def find(s)
    return {} if @entries.empty?
    res = {}
    @entries.each do |k, v|
      res[k] = v if k.include?(s) && !v.nil?
    end    
    res
  end
  
  def printable
    res = []
    @entries.keys.sort.each do |k|
      res << "[#{k}] \"#{@entries[k]}\""
    end
    res.join("\n")
  end
  
end
