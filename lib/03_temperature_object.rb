class Temperature
  
  def initialize(hash)
    @temp = hash
  end
  
  def in_fahrenheit
    @temp[:f] || self.class.ctof(@temp[:c])
  end
  
  def in_celsius
    @temp[:c] || self.class.ftoc(@temp[:f])
  end
  
  def self.from_fahrenheit(f)
    Temperature.new(:f => f)
  end
  
  def self.from_celsius(c)
    Temperature.new(:c => c)
  end
  
  def self.ctof(c)
    c * 9 / 5.0 + 32
  end
  
  def self.ftoc(f)
    (f - 32) * 5.0 / 9
  end
  
end

class Celsius < Temperature
  def initialize(c)
    @temp = {:c => c}
  end
end

class Fahrenheit < Temperature
  def initialize(f)
    @temp = {:f => f}
  end
end