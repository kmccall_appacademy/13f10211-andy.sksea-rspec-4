class Book
  attr_reader :title
  
  SMALL_WORDS = ["the", "an", "a", "and", "in", "of"]
  
  def title=(t)
    @title = t.split(" ").map.with_index do |w, i|
      i == 0 || !SMALL_WORDS.include?(w) ? w.capitalize : w
    end.join(" ")
  end  
end
